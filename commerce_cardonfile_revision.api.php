<?php

/**
 * @file
 * Hooks provided by the Commerce Card On File Revisions module.
 */

/**
 * Card Data Revision Insert
 *
 * @param $revision_data
 *   The inserted card revision data.
 */
function hook_commerce_cardonfile_revision_data_insert($revision_data) {
  // perform custom operations reacting to the creation of a new card revision data
}

/**
 * Card Data Revision Update
 *
 * @param $revision_data
 *   The saved card revision data.
  * @param $revision_data_original
 *   The original unchanged card revision data.
 */
function hook_commerce_cardonfile_revision_data_update($revision_data, $revision_data_original) {
  // perform custom operations reacting to the update of an existing
  // card revision data
}

/**
 * Card Data Revision Delete
 *
 * @param $revision_data
 *   The deleted card revision data.
 */
function hook_commerce_cardonfile_revision_data_delete($revision_data) {
  // perform custom operations reacting to the deletion of an existing
  // card revision data
}
