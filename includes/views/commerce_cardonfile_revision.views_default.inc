<?php

/**
 * @file
 * Views for the default Commerce Card on File revisions.
 */

/**
 * Implements hook_views_default_views().
 */
function commerce_cardonfile_revision_views_default_views() {
  $views = array();

// -----------------------------------------------------------------------
// User card revisions

  $view = new view();
  $view->name = 'commerce_user_cof_revisions';
  $view->description = 'Display a list of revisions for cards on file for a user.';
  $view->tag = 'default';
  $view->base_table = 'commerce_card_data_revision';
  $view->human_name = 'User Card on File Revisions';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Card Revisions';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'view own card data revisions';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'uid' => 'uid',
    'card_id' => 'card_id',
    'revision_id' => 'revision_id',
    'revision_timestamp' => 'revision_timestamp',
    'status' => 'status',
    'card_name' => 'card_name',
    'card_type' => 'card_name',
    'card_number' => 'card_name',
    'card_exp_month' => 'card_name',
    'card_exp_year' => 'card_name',
    'log' => 'log',
    'operations' => 'operations',
  );
  $handler->display->display_options['style_options']['default'] = 'revision_id';
  $handler->display->display_options['style_options']['info'] = array(
    'uid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'card_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'revision_id' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => 'views-align-center',
      'separator' => '',
      'empty_column' => 0,
    ),
    'revision_timestamp' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'status' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'card_name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '<br />',
      'empty_column' => 0,
    ),
    'card_type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'card_number' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'card_exp_month' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'card_exp_year' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'log' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'operations' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Commerce Card on File Revisions: Uid */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'commerce_card_data_revision';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  $handler->display->display_options['fields']['uid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['uid']['link_to_user'] = FALSE;
  /* Field: Commerce Card on File Revisions: Card ID */
  $handler->display->display_options['fields']['card_id']['id'] = 'card_id';
  $handler->display->display_options['fields']['card_id']['table'] = 'commerce_card_data_revision';
  $handler->display->display_options['fields']['card_id']['field'] = 'card_id';
  $handler->display->display_options['fields']['card_id']['exclude'] = TRUE;
  /* Field: Commerce Card on File Revisions: Card Revision ID */
  $handler->display->display_options['fields']['revision_id']['id'] = 'revision_id';
  $handler->display->display_options['fields']['revision_id']['table'] = 'commerce_card_data_revision';
  $handler->display->display_options['fields']['revision_id']['field'] = 'revision_id';
  $handler->display->display_options['fields']['revision_id']['label'] = 'Revision ID';
  $handler->display->display_options['fields']['revision_id']['separator'] = '';
  /* Field: Commerce Card on File Revisions: Revision timestamp */
  $handler->display->display_options['fields']['revision_timestamp']['id'] = 'revision_timestamp';
  $handler->display->display_options['fields']['revision_timestamp']['table'] = 'commerce_card_data_revision';
  $handler->display->display_options['fields']['revision_timestamp']['field'] = 'revision_timestamp';
  $handler->display->display_options['fields']['revision_timestamp']['label'] = 'Created';
  $handler->display->display_options['fields']['revision_timestamp']['date_format'] = 'short';
  /* Field: Commerce Card on File Revisions: Card data status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'commerce_card_data_revision';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  $handler->display->display_options['fields']['status']['label'] = 'Status';
  /* Field: Commerce Card on File Revisions: Name on the card */
  $handler->display->display_options['fields']['card_name']['id'] = 'card_name';
  $handler->display->display_options['fields']['card_name']['table'] = 'commerce_card_data_revision';
  $handler->display->display_options['fields']['card_name']['field'] = 'card_name';
  $handler->display->display_options['fields']['card_name']['label'] = 'Card information';
  /* Field: Commerce Card on File Revisions: Card type */
  $handler->display->display_options['fields']['card_type']['id'] = 'card_type';
  $handler->display->display_options['fields']['card_type']['table'] = 'commerce_card_data_revision';
  $handler->display->display_options['fields']['card_type']['field'] = 'card_type';
  /* Field: Commerce Card on File Revisions: Card number */
  $handler->display->display_options['fields']['card_number']['id'] = 'card_number';
  $handler->display->display_options['fields']['card_number']['table'] = 'commerce_card_data_revision';
  $handler->display->display_options['fields']['card_number']['field'] = 'card_number';
  $handler->display->display_options['fields']['card_number']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['card_number']['alter']['text'] = '******[card_number]';
  /* Field: Commerce Card on File Revisions: Card expiration month */
  $handler->display->display_options['fields']['card_exp_month']['id'] = 'card_exp_month';
  $handler->display->display_options['fields']['card_exp_month']['table'] = 'commerce_card_data_revision';
  $handler->display->display_options['fields']['card_exp_month']['field'] = 'card_exp_month';
  $handler->display->display_options['fields']['card_exp_month']['exclude'] = TRUE;
  $handler->display->display_options['fields']['card_exp_month']['separator'] = '';
  /* Field: Commerce Card on File Revisions: Card expiration year */
  $handler->display->display_options['fields']['card_exp_year']['id'] = 'card_exp_year';
  $handler->display->display_options['fields']['card_exp_year']['table'] = 'commerce_card_data_revision';
  $handler->display->display_options['fields']['card_exp_year']['field'] = 'card_exp_year';
  $handler->display->display_options['fields']['card_exp_year']['label'] = 'Card expiration';
  $handler->display->display_options['fields']['card_exp_year']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['card_exp_year']['alter']['text'] = '[card_exp_month]/[card_exp_year]';
  $handler->display->display_options['fields']['card_exp_year']['separator'] = '';
  /* Field: Commerce Card on File Revisions: Revision log */
  $handler->display->display_options['fields']['log']['id'] = 'log';
  $handler->display->display_options['fields']['log']['table'] = 'commerce_card_data_revision';
  $handler->display->display_options['fields']['log']['field'] = 'log';
  $handler->display->display_options['fields']['log']['label'] = 'Log';
  /* Field: Commerce Card on File Revisions: Operations links */
  $handler->display->display_options['fields']['operations']['id'] = 'operations';
  $handler->display->display_options['fields']['operations']['table'] = 'commerce_card_data_revision';
  $handler->display->display_options['fields']['operations']['field'] = 'operations';
  $handler->display->display_options['fields']['operations']['label'] = 'Operations';
  $handler->display->display_options['fields']['operations']['add_destination'] = 0;
  /* Contextual filter: Commerce Card on File Revisions: Uid */
  $handler->display->display_options['arguments']['uid']['id'] = 'uid';
  $handler->display->display_options['arguments']['uid']['table'] = 'commerce_card_data_revision';
  $handler->display->display_options['arguments']['uid']['field'] = 'uid';
  $handler->display->display_options['arguments']['uid']['default_action'] = 'access denied';
  $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Commerce Card on File Revisions: Card ID */
  $handler->display->display_options['arguments']['card_id']['id'] = 'card_id';
  $handler->display->display_options['arguments']['card_id']['table'] = 'commerce_card_data_revision';
  $handler->display->display_options['arguments']['card_id']['field'] = 'card_id';
  $handler->display->display_options['arguments']['card_id']['default_action'] = 'access denied';
  $handler->display->display_options['arguments']['card_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['card_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['card_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['card_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['card_id']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['card_id']['validate']['type'] = 'commerce_cof_revision_card_id';
  $handler->display->display_options['arguments']['card_id']['validate']['fail'] = 'access denied';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'user/%/stored-payment-methods/%/revisions';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Revisions';
  $handler->display->display_options['menu']['weight'] = '20';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 1;


  $views[$view->name] = $view;


  return $views;
}
