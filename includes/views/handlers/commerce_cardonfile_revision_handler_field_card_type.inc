<?php

/**
 * Field handler to translate a card type name into its readable form.
 */
class commerce_cardonfile_revision_handler_field_card_type extends views_handler_field {
  function render($values) {
    $name = $this->get_value($values);

    // Load the credit card helper functions from the Payment module.
    module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');
    $card_types = commerce_payment_credit_card_types();

    // Extract the name of the card type if possible.
    if (!empty($card_types[$name])) {
      return $this->sanitize_value($card_types[$name]);
    }
  }
}
