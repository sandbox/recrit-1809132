<?php

/**
 * @file
 * Views integration for Commerce Card on File Revisions.
 */

/**
 * Implements hook_views_data()
 */
function commerce_cardonfile_revision_views_data() {
  $data = array();

  // -----------------------------------------------------------------------
  // Base table setup

  $data['commerce_card_data_revision']['table']['group']  = t('Commerce Card on File Revisions');
  $data['commerce_card_data_revision']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Commerce Card on File Revisions'),
    'help' => t('Saves information about each saved revision of card data.'),
  );

  // For other base tables, explain how we join
  // 'left_field' is the primary key in the referenced table.
  // 'field' is the foreign key in this table.

  $data['commerce_card_data_revision']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
/*
 @todo: currently there is no commerce_card_data views support
    'commerce_card_data' => array(
      'left_field' => 'card_id',
      'field' => 'card_id',
    ),
*/
  );

  // Expose Fields

  // Card ID.
  $data['commerce_card_data_revision']['card_id'] = array(
    'title' => t('Card ID'),
    'help' => t('The unique internal identifier of the card data.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_card_data',
      'field' => 'card_id',
      'label' => t('Card Data', array(), array('context' => 'credit card data')),
    ),
  );

  // Revision ID.
  $data['commerce_card_data_revision']['revision_id'] = array(
    'title' => t('Card Revision ID'),
    'help' => t('The card data revision ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  $data['commerce_card_data_revision']['revision_timestamp'] = array(
    'title' => t('Revision timestamp'),
    'help' => t('The date when this revision was created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // Revision log
  $data['commerce_card_data_revision']['log'] = array(
    'title' => t('Revision log'),
    'help' => t('The log entry explaining the changes in this version.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // Card owner uid.
  $data['commerce_card_data_revision']['uid'] = array(
    'title' => t('Uid'),
    'help' => t("The owner's user ID."),
    'field' => array(
      'handler' => 'views_handler_field_user',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
      'name field' => 'name', // display this field in the summary
    ),
    'filter' => array(
      'title' => t('Name'),
      'handler' => 'views_handler_filter_user_name',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'title' => t('Owner'),
      'help' => t("Relate this card revision to its owner's user account"),
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'field' => 'uid',
      'label' => t('Card revision owner'),
    ),
  );

  // Card remote id
  $data['commerce_card_data_revision']['remote_id'] = array(
    'title' => t('Card data remote ID'),
    'help' => t('The remote ID to the full card data at the payment gateway.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Card card_type
  $data['commerce_card_data_revision']['card_type'] = array(
    'title' => t('Card type'),
    'help' => t('The credit card type.'),
    'field' => array(
      'handler' => 'commerce_cardonfile_revision_handler_field_card_type',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Card card_name
  $data['commerce_card_data_revision']['card_name'] = array(
    'title' => t('Name on the card'),
    'help' => t('The name on the credit card.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Card card_number
  $data['commerce_card_data_revision']['card_number'] = array(
    'title' => t('Card number'),
    'help' => t('Truncated number of the credit card (last 4 digits).'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Card expiration month
  $data['commerce_card_data_revision']['card_exp_month'] = array(
    'title' => t('Card expiration month'),
    'help' => t('Expiration month of the credit card.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Card expiration year
  $data['commerce_card_data_revision']['card_exp_year'] = array(
    'title' => t('Card expiration year'),
    'help' => t('Expiration year of the credit card.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );


  // Card status
  $data['commerce_card_data_revision']['status'] = array(
    'title' => t('Card data status'),
    'help' => t('Status of the card data storage.'),
    'field' => array(
      'handler' => 'commerce_cardonfile_revision_handler_field_card_status',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Operation links
  $data['commerce_card_data_revision']['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'help' => t('Display all the available operations links for the card revisions.'),
      'handler' => 'commerce_cardonfile_revision_handler_field_data_operations',
    ),
  );


  return $data;
}

/**
 * Implements hook_views_plugins
 */
function commerce_cardonfile_revision_views_plugins() {
  return array(
    'argument validator' => array(
      'commerce_cof_revision_card_id' => array(
        'title' => t('Revision Card ID Access'),
        'handler' => 'commerce_cardonfile_revision_plugin_argument_validate_card_id',
      ),
    ),
  );
}
