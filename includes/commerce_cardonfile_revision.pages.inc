<?php

/**
 * @file
 * Page callbacks and forms for Commerce Card on File Revisions.
 */

/**
 * Builds the form for deleting cardonfile data revisions.
 *
 * @param $card_data
 *   The data array representing a card on file revision.
 */
function commerce_cardonfile_revision_delete_form($form, &$form_state, $card_revision_data) {
  $form['card_revision_data'] = array(
    '#type' => 'value',
    '#value' => $card_revision_data,
  );

  return confirm_form(
    $form,
    t('Are you sure you want to delete this card data revision?'),
    'user/' . $card_revision_data['uid'] . '/stored-payment-methods/' . $card_revision_data['card_id'] . '/revisions',
    theme('card_data_overview', array('card_data' => $card_revision_data)),
    t('Delete')
  );
}

/**
 * Form submit handler: delete stored card data.
 */
function commerce_cardonfile_revision_delete_form_submit($form, &$form_state) {
  $card_revision_data = $form_state['values']['card_revision_data'];

  commerce_cardonfile_revision_data_delete($card_revision_data);

  drupal_set_message(t('The stored card revision has been deleted.'));

  // Redirect to the payment methods tab if the user has other stored payment.
  $card_revisions = commerce_cardonfile_revision_data_load_by_card($card_revision_data['card_id']);

  if (!empty($card_revisions)) {
    $form_state['redirect'] = 'user/' . $card_revision_data['uid'] . '/stored-payment-methods/' . $card_revision_data['card_id'] . '/revisions';
  }
  else {
    $form_state['redirect'] = 'user/' . $card_revision_data['uid'] . '/stored-payment-methods';
  }
}
